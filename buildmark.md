# This file mark some packages with some different things

This is a file mark package(s) building abnornmal. 

Some packages can't build as normal with default option(s) as offical PKGBUILD files, with other package can't pass test process while building with PKGBUILD file or source code. 

## Package PKGBUILD file different from offical:

- `systemd`: remove some depends
- `pixman`: disable some options
- `mkinitcpio-busybox`: delete arch and platform option
- `valgrind`: delete 32bit ABI depends and support
- `python`: disable some options
- `openssl`: speical change configure cpu arch
- `vim`: remove some depends
- `ruby`: remove some depends
- `python`: remove some depends
- `python2`: remove some depends
- `cairo`: disable gtk-doc option
- `cmake`: change configure to -no-qt-gui
- `llvm`: remove depends about docs and ocmal, don't build anything about docs
- `boost`: disable all option about ocaml and mpi
- `openssh`: remove linux-headers depend
- `lld`: redefind compiler to clang but no gcc by changing source code's CMakeLists.txt
- `freetype2`: remove some depends, don't build freetype2-demo, take down some command
- `python-fonttools`: disable check and remove check depend, to prevent depend loop
- `harfbuzz`: disable graphlite2, docs, chafa options manual
- `ffcall`: force change pre-compile assembly source code to pass building
- `gobject-introspection`: disable gtk-doc option
- `rust`: remove `rust-musl` `rust-wasm` package build because offical unsupport on mipsisa64r6. change config.toml's build target.
- `mesa`: remove `vulkan-intel` `vulkan-radeon` `vulkan-swrast` package build because unsupport on mips64r6.
- `wayland`: disable document option
- `rrdtool`: source tarball address code changed, so we update it manually to 1.8.0
- `yajl`: source tarball address code changed, so we fix it manually
- `libtheora`: specify build host manually
- `libepoxy`: remove doxygen depend
- `aom`: remove doxygen depend
- `lapack`: remove doxygen depend, and lapack-docs packgae-build
- `dbus`: remove doxygen depend
- `glib2`: remove `gtk-doc` `libsysprof-capture` depend, disable related options; disable tests option, because it can't pass test now
- `hdf5`: disabled all options aand depend about java
- `libxkbcommon`: disable document because remove doxygen depend
- `lensfun`: remove `doxygen` depend
- `hspell`: remove `qt6-webengine` and it's command
- `nuspell`: remove `pandoc` depend
- `rust`: build in speical way (with a different config.toml)
- `ding-libs`: remove `doxygen` and `check` depend
- `gssproxy`: remove `doxygen` depend
- `filesystem`: changed lib64 folder judge condition
- `openjpeg2`: remove `doxygen` depend
- `libsigc++`: disble documention option because leak of `doxygen` depend
- `dav1d`: disble documention option because leak of `doxygen` depend
- `libimagequant`: add '--disable-sse' compile option
- `cppunit`: remove `doxygen` depend
- `python-matplotlib`: remove all optional depends
- `nss`: change target type to mips64
- `libteam`: remove `doxygen` depend
- `libbluray`: remove `java` depend, add '--disable-bdjava-jar' compile option
- `libsamplerate`: remove `fftw` depend
- `vmaf`: remove `doxygen` depend
- `fmt`: remove `doxygen` depend and npm plugin install
- `libssh`: remove `doxygen` depend
- `vid.stab`: remove sse2 compile option manually
- `libavif`: remove `rav1e` depend, turn off rav1e option
- `libheif`: remove `libde265` depend
- `v4l-utils`: remove `qt5-base` depend
- `graphite`: remove `doxygen` depend
- `liblo`: remove `doxygen` depend
- `libltc`: remove `doxygen` depend
- `libsrtp`: remove `doxygen` depend and disable doc option
- `gavl`: remove `doxygen` depend and add '--without-doxygen option' whe configure
- `modemmanager` & `ibmm-glib`: remove `polkit` depend and add change option 'polkit' to no
- `librsvg`: use the last version 2.40.20 unneed `rust` <!-- ababf709bade430b3bc182d25d9969f734fb2e1b -->
- `libical`: remove `doxygen` depend
- `librevenge`: remove `doxygen` depend
- `libwpd`: remove `doxygen` depend
- `libwpg`: remove `doxygen` depend
- `libodfgen`: remove `doxygen` depend
- `libvisio`: remove `doxygen` depend
- `libfreehand`: remove `doxygen` depend
- `libqxp`: remove `doxygen` depend
- `libmspub `: remove `doxygen` depend
- `libpagemaker`: remove `doxygen` depend
- `libcdr `: remove `doxygen` depend
- `ghostscript`: remove `gtk3` depend
- `cups`: remove `cups-filters` `colord` `avahi` depend
- `gtk2`: remove `gtk-update-icon-cache` depend
- `libopenraw`: use the last version 0.1.3-2 unneed `cargo` <!-- 6d4332452fa143641339f0ee53d79bbffaa33efd -->
- `imagemagick`: remove `libjxl` depend, delete libjxl's option
- `imlib2`: delete '--enable-amd64' option
- `lv2 `: remove `doxygen` depend
- `polkit`: use the last version 0.1.3-2 unneed `js` <!-- 8ff3c3c9e8c80fea33d1d1a5aa3eea41edc59eb7 -->
- `subversion `: arhclinux's version is outdated, so we update it manually to 1.14.2; remove `kwallet` `kdelibs4support` `java` depends, delete options about java(hl) and '--with-kwallet' 
- `fuse2 `: build with glibc-2.32, because it has bug with higer version's glibc
- `libsecret `: remove `gjs` depend
- `python-wsaccel`: change check command's path to mips64

<!-- - `texlive-bin`: remove `clisp` depend -->
<!-- clisp`: force change source from hg to tarball, because hg can't download success -->
<!-- poppler`: remove 'qt5-base' 'qt6-base' depend, and it's related packgae-build -->
<!-- sane`: remove 'avahi' depend and it's compile options temporaily -->

## Package test fail while building with PKGBUILD file:
- `dbus-broker`
- `libevent`
- `ctag`
- `gtest `
- `systemd`
- `pixman`: cause by timeout. maybe fix by turn on the mips-dsp option
- `perl`
- `perl-dbd-mariadb`
- `perl-dbd-mysql`
- `leatherman`
- `libdwarf`
- `openssl`
- `opensp`
- `fuse2`
- `vim`
- `llvm`
- `clang`
- `lld`
- `libtool`
- `python-cairo`: may be cause by not full support of harfbuzz.
- `ffcall`
- `valgrind`: can't identify mips64r6, so it doesn't process check
- `wayland`
- `libwacom`
- `python-snappy`
- `python-greenlet`: seems that test.py have some problems
- `libxkbcommon`: skiped 1 test
- `coreutils`
- `json-glib`: timeout 1 test
- `at-spi2-core`
- `gsl`
- `python-mpi4py`
- `python-pycapnp`
- `givaro`
- `python-pyzmq`
- `rcs`
- `ksh`
- `sudo`
- `libjpeg-turbo`
- `dav1d`
- `python-cryptography`: cause by `cargo`/`rust`
- `python-pillow`
- `python-pandas`
- `libgudev`
- `hotdoc`
- `gdk-pixbuf2`
- `at-spi2-atk`
- `dconf`
- `gd`
- `xmlsec`
- `python-bcrypt`
- `libgit2-glib`
- `libsrtp`
- `libltc`
- `libgsf`
- `babl`
- `tbb`: test 23/118 will cause timeout 
- `gdk-pixbuf`: test 23/23 failed
- `tpm2-abrmd`: XFAIL: test/integration/start-auth-session.int
- `libsecret`: test timeout
- `swtpm`: test timeout
- `python-dulwich`
- `python-wsaccel`

## Package test fail while building with source code:
```
```

## Package built in dirty-way so need to repack in the future:
```
cairo
pango
freetype2
fontconfig
harfbuzz
mesa
libva
wayland
gobject-introspection
glib2
libwacom
dav1d
rust
graphite
```

## Package build fail that need to fix upstream source code:

- `ffcall`: need to update it's pre-compile's gcc version, and add '-mno-branch-likely' option (temporaily built success by fix it manually)
- `libjpeg-turbo`: compiled failed caused by assembly part about mips and loongson fixed (temporaily built success by FlyGoats's patch, waiting for upstream accepting FlyGoats's PR)
- `lldb`: need to transplant some code
- `valgrind`: configure & make file have unnecessary "-march=mips64r2" tag options with mips (temporaily built success by fix it manually)
- `nodejs`: unsupport mips r6 now
- `openblas`: compiled failed caused by assembly part about mips and loongson fixed (waiting for upstream accepting FlyGoats's PR)
- `fuse2`: PKGBUILD file's version outdate of source code
- `aalib`: config.guess type error 
- `sbc`: cpu init function error by ld . "/usr/sbin/ld`: sbc_primitives.c:(.text+0x2c7c)`: undefined reference to `__builtin_cpu_init'"
- `dsniff`: had a type error in 'pcaputil.h', need to fix by upstream
- `libmad`: some problem with assembly command in .h file
- `sbcl`: mipsel-binfmt-P: Could not open '/lib/ld.so.1'`: No such file or directory
- `gfan`: because header file of cddlib moved, it reported no such header files. need to fix by upstream
- `c-xsc`: unknown error. ../../src/interval.inl:136:58: error: ISO C++17 does not allow dynamic exception specifications
- `gf2x`:  config.guess type error 
- `a52dec`:  config.guess type error (temporaily built success by fix it manually)
- `zn_poly`: some problem with assembly command in .h file
- `libfabric`: only support x86 platform. ./prov/opx/include/rdma/opx/fi_opx_timer.h:66:2: error: #error "Cycle timer not defined for this platform"
- `rav1e`: cause by rust
- `mono`: still unsupport 64-bit on mips. need to transplant
- `devil`: have a conflict variable 'mips' with gcc (temporaily built success by fix it manually)
- `avisynthplus`: need to transplant. unsupport mips at all.
- `fftw`: compile options problems
- `clisp`: assembly part about mips unsupport r6
- `chmlib`: leak of some include header file
- `SVT-HEVC`: use x86's header file
- `c-xsc`: unknown error. Aiksaurus.cpp:101:17: error: ISO C++17 does not allow dynamic exception specifications
- `lshw`: archlinux's upsteream leak of depend


## Package build fail with other reasons:

- `libgme`: leak of gcc-libs `ubsan`, /usr/sbin/ld: cannot find -lubsan
- `texlive-bin`: Error: pointer size mismatch in cross-build.
