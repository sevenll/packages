# Maintainer: Sébastien Luttringer

pkgname=lz4
epoch=1
pkgver=1.9.3
pkgrel=2
pkgdesc='Extremely fast compression algorithm'
arch=('mipsisa64r6el')
url='http://www.lz4.org/'
license=('GPL2')
makedepends=('git')
checkdepends=('diffutils')
depends=('glibc')
source=("https://github.com/lz4/lz4/releases/download/v${pkgver}/lz4-${pkgver}.tar.gz"{,.sig}
        'https://github.com/lz4/lz4/commit/8301a21773ef61656225e264f4f06ae14462bca7.patch')
sha256sums=('030644df4611007ff7dc962d981f390361e6c97a34e5cbc393ddfbe019ffe2c1'
            'SKIP'
            'a9109fac17ee72ed3ec86f2cd5e8c4a0f34a548213a3d3c89861d6b2a6bd2871')

prepare() {
  cd $pkgname-$pkgver
  # apply patch from the source array (should be a pacman feature)
  local src
  for src in "${source[@]}"; do
    src="${src%%::*}"
    src="${src##*/}"
    [[ $src = *.patch ]] || continue
    msg2 "Applying patch $src..."
    patch -Np1 < "../$src"
  done
}

build() {
  # do not use the main makefile, it calls sub make with -e
  # exported CLFAGS by makepkg break the version. see FS#50071
  cd $pkgname-$pkgver
  make -C lib PREFIX=/usr
  make -C programs PREFIX=/usr lz4 lz4c
}

check() {
  rm -f passwd.lz4
  $pkgname-$pkgver/programs/lz4 /etc/passwd passwd.lz4
  $pkgname-$pkgver/programs/lz4 -d passwd.lz4 passwd
  diff -q /etc/passwd passwd
  rm passwd
}

package() {
  cd $pkgname-$pkgver
  make install PREFIX=/usr DESTDIR="$pkgdir"
}

# vim:set ts=2 sw=2 et:
