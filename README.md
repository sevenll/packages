# Arch Linux MIPS r6 Package Repositories

This is the `PKGBUILD` collection for the Arch Linux MIPS r6 repositories.

## Repositories

Just like Arch Linux, this project consists of three main repositories: `core`, `extra`, and `community`. These three repositories contain recompiled Arch Linux packages. While Arch Linux MIPS r6 strives to achieve feature parity with Arch Linux, this project is still in the growing phase so certain features of certain packages might be intentionally disabled to help with compiling them. Other features (such as GCC with Ada support) are omitted due to lack of support on the MIPS r6 architecture. Package versions might also differ due to complications related to architecture support or regressions on MIPS r6.

There is also an `upstream` repository, which includes certain `any` packages from Arch Linux, that do not need to be recompiled. 

Because we decided add all package with arch `any` to our repositority, so we don't need to maintain a `upstream.txt` file. We will add dependency package of `upstream` asap.

After 2022/4, we also add an `upstream-community` repository, includes all certain `any` & `community` packages from Arch Linux, that do not need to be recompiled.

## Configuration

These repositories are enabled by default. A list of code signing keys can be found at the [keyring](https://gitlab.com/arch-linux-mipsr6/keyring) repository and the keyring package can be found at `core/archmipsr6-keyring`. A list of repo servers can be found at `core/pacman-mirrorlist`.